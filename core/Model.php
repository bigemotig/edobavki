<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 22.10.16
 * Time: 17:21
 */

class ModelRecord {
    public $info = array();
}

class ModelView {

}

class Model {
    public $data = null;
    protected $db = null;
    protected $table = '';
    private $record_class_name = '';

    public function __construct($db, $data) {
        $this->db = $db;
        $this->record_class_name = get_class($this) . 'Record';
        $this->data = new $this->record_class_name();
        $this->fill($data);
    }

    public function fill($data = array()){
        if (is_null($data))
            return;
        foreach ($data as $key=>$value) {
            if(property_exists($this->data, $key))
                $this->data->$key = $value;
            else
                if(property_exists($this->data, 'info'))
                    $this->data->info[$key] = $value;
        }
    }

    /**
     * Серверный валидатор
     * @return array
     */
    public function validate() {
        return array();
    }

}

class Factory {
    protected $table = '';
    protected $default_order_field = 'spec';
    protected $db = null;
    public $row_data = array();
    public $model = null;

    public function __construct($db) {
        $this->db = $db;
    }

    public function get($id) {
        $result = $this->get_list(array('filters' => array(
            'id' => $id
        )));
        $row_data = $result->row_data;
        $model = !empty($row_data) ? reset($row_data) : null;
        return $model;
    }

    public function get_list($args = array()) {
        $sql = isset($args['sql']) ? $args['sql'] : new SqlWriter($this->table);

        if (!empty($args['filters']))
            $sql->set_filters($args['filters']);

        if (!empty($args['orders']))
            $sql->set_orders($args['orders']);

        $sql_str = $sql->get_sql();
        $result = $this->get_all($sql_str);

        return $result;
    }

    public function get_all($sql) {
        $db = &$this->db;
        $db->query($sql);
        $this->row_data = $db->get_rows();

        return $this;
    }

    public function as_array() {
        return $this->row_data;
    }

    public function as_selector($fields = array('id', 'spec')) {
        $selector = array();
        foreach ($this->row_data as $row) {
            $temp_row = array();
            foreach ($row as $field=>$value) {
                if (in_array($field, $fields))
                    $temp_row[$field] = $value;
            }
            $selector[] = $temp_row;
        }
        return $selector;
    }

    public function as_ids($field = 'id') {
        $ids = array();
        foreach ($this->row_data as $row) {
            if (isset($row[$field]))
                $ids[] = $row[$field];
        }
        return $ids;
    }

    public function order_filters($args = array()) {

    }

    /**
     * @param array $filters
     * @param SqlWriter $sql
     * @return SqlWriter $sql
     */
    public function filters_convert($filters, $sql) {
        $convert_filters = array();
        foreach ($filters as $filter) {

        }

        return $sql;
    }
}

class Map extends Model{

}

class MapFactory extends Factory{

}
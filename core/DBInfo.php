<?php

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 29.10.16
 * Time: 16:05
 */

/**
 * @param mysql $db
 */
class DBInfo {
    private $_db;
    private $_db_current;
    public $table_alias = array();
    public function __construct($db) {
        $config = $db->get_config();
        $this->_db = new PDO($config['schema-dsn'], $config['dblogin'], $config['dbpswd'], array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8'
        ));
        $this->_db_current = $config['dbname'];

        $this->create_aliases();
    }

    private function create_aliases() {
        $sql = "SELECT TABLE_NAME, TABLE_COMMENT FROM TABLES WHERE TABLE_SCHEMA = '{$this->_db_current}'";
        $res = $this->_db->query($sql);
        $rows = $res->fetchAll();

        foreach ($rows as $row) {
            $json = json_decode($row['TABLE_COMMENT']);
            $this->table_alias[$row['TABLE_NAME']] = $json->alias;
        }
    }
}
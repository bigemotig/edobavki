<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 29.10.16
 * Time: 14:41
 */
class SqlWriter {
    private $_table = '';
    private $_alias_table = '';
    private $_alias = '';
    private $_type = 'SELECT';
    private $_distinct = false;
    private $_field = array();
    private $_extra_field = array();
    private $_join = array();
    private $_where = array();
    private $_having = array();
    private $_group = array();
    private $_order = array();
    private $_limit = '';
    private $_db = null;
    private $_db_info = null;
    private $_sql = '';
    private $_table_alias = array();


    public function __construct($table = '', $alias = null) {
        $this->_table = $table;
        $this->_db = core::$db;
        $this->_db_info = $this->_db->db_info();
        $this->_table_alias = $this->_db_info->table_alias;
        $this->_alias_table = $this->alias($table);
    }

    public function set_field($field) {
        if (is_array($field)) {
            $this->_field = $field;
        }
        else {
            if (!empty($field))
                $this->_field = $field;
        }
        return $this;
    }

    public function set_extra_field($extra_field) {
        if (is_array($extra_field)) {
            $this->_extra_field[] = $extra_field;
        }
        else {
            if (!empty($extra_field))
                $this->_extra_field[] = $extra_field;
        }
        return $this;
    }

    public function set_join($join) {
        if (is_array($join)) {
            $this->_join[] = $join;
        }
        else {
            $this->_join[]['join_table'] = $join;
        }
        return $this;
    }

    public function set_order($order) {
        if (is_array($order)) {
            $this->_order[] = $order;
        }
        else {
            if (!empty($order))
                $this->_order[] = $order;
        }
        return $this;
    }

    public function set_limit($limit) {
        if (is_numeric($limit))
            $this->_limit = $limit;
        return $this;
    }

    public function set_where($where) {
        if (is_array($where)) {
            if (array_key_exists('connector', $where) || array_key_exists('field', $where))
                $this->_where[] = $where;
            else
                foreach ($where as $w) {
                    $this->_where[] = $w;
                }
        }
        else
            $this->_where = $where;
        return $this;
    }

    private function alias($table) {
        if (!empty($this->_alias))
            return $this->_alias;
        return $table == '' ? $table : $this->_table_alias[$table];
    }

    public function get_sql() {
        $this->_sql = $this->_type . ' ';
        $this->_sql .= $this->_distinct ? ' DISTINCT ' : ' ';
        $alias = $this->alias($this->_table);
        $this->_sql .= !empty($this->_field) ? $this->prepare_field() : ' * ';
        $this->_sql .= ' FROM ' . $this->_table . ' ' . $alias . ' ';
        if (!empty($this->_join))
            $this->_sql .= $this->prepare_join();

        if (!empty($this->_where)) {
            $this->_sql .= ' WHERE ';
            $this->_sql .= $this->prepare_where();
        }
        if (!empty($this->_order)) {
            $this->_sql .= ' ORDER BY ';
            $this->_sql .= $this->prepare_order();
        }
        if (!empty($this->_limit)) {
            $this->_sql .= ' LIMIT ';
            $this->_sql .= $this->prepare_limit();
        }
        return $this->_sql;
    }

    public function set_filters($filters) {
        $where = array();

        foreach ($filters as $field=>$value) {
            if (is_array($value))
                $where[] = array('table' => $this->_table, 'field' => $field, 'compare' => 'IN', 'value' => $value);
            else
                $where[] = array('table' => $this->_table, 'field' => $field, 'value' => $value);
        }
        $this->set_where($where);
    }

    public function set_orders($orders) {
        foreach ($orders as $key => $order) {
            $this->_order[] = array(
                'field' => $key,
                'sort' => $order
            );
        }
        return $this;
    }

    private function prepare_where() {
        $all_where = '';
        $where = array();
        $connector = 'AND';
        foreach ($this->_where as &$w) {
            if (isset($w['connector'])) {
                $connector = $w['connector'];
                unset($w['connector']);

                foreach ($w as $sub_row) {
                    $compare = isset($sub_row['compare']) ? $sub_row['compare'] : ' = ';
                    $where[] = $this->create_where_row($compare, $sub_row);
                }

            } else {
                $compare = isset($w['compare']) ? $w['compare'] : ' = ';
                $where[] = $this->create_where_row($compare, $w);
            }
        }
        $this->_where = array();
        $all_where .= implode(' ' . $connector . ' ', $where);
        return $all_where;
    }

    private function create_where_row($compare,  $w) {
        $where = ' ' . $this->alias($w['table']) . '.' . $w['field']
            . $this->get_where_value($compare, $w['value']);
        return $where;
    }

    private function get_where_value($compare, $value) {
        $cmpr = strtolower($compare);
        $cmpr = trim($cmpr);
        $where_value = '';
        switch ($cmpr) {
            case '=':
                if (is_null($value)) {
                    $where_value .= ' IS NULL ';
                }
                else
                    $where_value .= ' ' . $compare . '"'. $value . '" ';
                break;
            case 'in':
                $where_value .= ' ' . $compare . ' ('. implode(', ', $value) . ') ';
                break;
            default:
                $where_value .= ' ' . $compare . '"'. $value . '" ';
                break;
        }
        return $where_value;
    }

    private function prepare_field() {
        $all_field = '';
        $field = array();
        foreach ($this->_field as $key=>$f) {
            $field[] = $this->_alias_table . '.' .$f;
        }
        if (!empty($this->_extra_field))
            $field = array_merge($field, $this->prepare_extra_field());

        $this->_field = array();
        $this->_extra_field = array();
        $all_field .= implode(', ', $field);
        return $all_field;
    }

    private function prepare_extra_field() {
        $extra_field = array();
        foreach ($this->_extra_field as $ex) {
            $row = '';
            $table_alias = isset($ex['table']) ? $this->alias($ex['table']) : $this->_alias_table;
            $row .= $table_alias . '.' . $ex['field'];
            $row .= isset($ex['as']) ? ' as ' . $ex['as'] : '';
            $extra_field[] = $row;
        }
        $this->_extra_field = array();
        return $extra_field;
    }

    private function prepare_order() {
        $all_order = '';
        $order = array();
        foreach ($this->_order as $o) {
            $row = '';
            $table_alias = $this->get_order_alias($o);

            if ($table_alias == 'none') {
                $row .= $o['field'];
                $row .= isset($o['sort']) ? ' ' . $o['sort'] : ' DESC ';
            }
            else {
                $row .= $table_alias . '.' . $o['field'];
                $row .= isset($o['sort']) ? ' ' . $o['sort'] : ' DESC ';
            }
            $order[] = $row;
        }
        $this->_order = array();
        $all_order .= implode(', ', $order);
        return $all_order;
    }

    private function get_order_alias($o) {
        if (isset($o['alias']))
            $table_alias = $o['alias'];
        elseif (isset($o['table']))
            $table_alias = $this->alias($o['table']);
        else
            $table_alias = $this->_alias_table;

        return $table_alias;
    }

    private function prepare_limit() {
        $limit = '';
        if (is_array($this->_limit))
            $limit .= $this->_limit[0] . ',' .$this->_limit[1];
        else
            $limit .= '0,' . $this->_limit;

        return $limit;
    }

    private function prepare_join() {
        $all_join = ' ';
        foreach ($this->_join as $j) {
            $type = isset($j['type']) ? $j['type'] : 'INNER';
            $join_alias = $this->alias($j['join_table']);
            $target_alias = isset($j['target_table']) ? $this->alias($j['target_table']) : $this->_alias_table;
            $all_join .= $type . ' JOIN ';
            $all_join .= $j['join_table'] . ' ' . $join_alias .' ';
            $all_join .= 'ON ' . $join_alias . '.' . $j['join_field'] .' = ' . $target_alias . '.' . $j['target_field'] . ' ';
        }
        $this->_join = array();
        return $all_join;
    }
}
<?php
require_once 'DBInfo.php';
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 23.10.16
 * Time: 10:59
 */
class DatabaseManager{
    private $_connections = array();
    private $_config = array();
    private $_options = array();

    /**
     * DataBaseManager constructor.
     * @param array $config
     */
    public function __construct($config){
        $this->_config = $config;
        $this->_options = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8'
        );
    }

    public function get_connection($name){
        if (isset($this->_connections[$name]))
            return $this->_connections[$name];
        if (!isset($this->_config[$name]))
            throw new Exception('Описание соединения '.$name.'отсутствует');

        $db_config = $this->_config[$name];
        //mysql:host=localhost;dbname=testdb
        $dsn = 'mysql:'; //префикс
        if (isset($db_config['dbhostname'])) {
            $dsn .= 'host=' . $db_config['dbhostname'] . ';'; //
            if (isset($db_config['dbport']))
                $dsn .= 'port=' . $db_config['dbport'] . ';';
        } elseif (isset($db_config['dbunixsocket']))
            $dsn .= 'unix_socket=' . $db_config['dbunixsocket'] . ';';

        $data_dsn = $dsn . 'dbname=' . $db_config['dbname'];
        $schema_dsn = $dsn . 'dbname=information_schema';
        $db_config['dsn'] = $dsn;
        $db_config['schema-dsn'] = $schema_dsn;
        $connection = new PDO($data_dsn, $db_config['dblogin'], $db_config['dbpswd'], $this->_options);
        $db = new mysql($connection, $db_config);
        $this->_connections[$name] = $db;
        return $this->_connections[$name];
    }
}

class mysql {
    private $_db = null;
    private $_config = null;
    private $_schema = null;
    private $sql = '';

    public $result = null;
    public $rows = array();
    public $num_rows = 0;
    public $write_log = 1;
    public $calc_rows = 0;
    public $logs = array();

    public function __construct(PDO $db, $config = array()) {
        $this->_db = $db;
        $this->_config = $config;
    }

    public function get_config() {
        return $this->_config;
    }

    public function db_info() {
        if (is_null($this->_schema))
            $this->_schema = new DBInfo($this);
        return $this->_schema;
    }

    public function query($sql) {
        $this->result = null;
        $this->sql = $sql;

        try {
            $this->result = $this->_db->query($this->sql);

            if (SQL_WRITER_DEBUG == 1)
                file_put_contents(SQL_LOG_PATH, $this->sql, FILE_APPEND);

        } catch (PDOException $pdo_ex) {
            error_log($this->sql);
            throw $pdo_ex;
        }
    }

    public function get_rows() {
        $this->rows = $this->result->fetchAll();
        return $this->rows;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 22.10.16
 * Time: 17:10
 */
class core {
    public static $config = null;
    public static $auth_user = null;
    /**
     * @var mysql
     */
    public static $db = null;
    public static $master_db = null;
    /**
     * @var Smarty $tpl
     */
    public static $tpl = null;
    public static $controller = null;
    public static $db_manager = null;
}
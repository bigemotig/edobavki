<?php
use _common\model\map\SeoPageFactory;

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 23.10.16
 * Time: 12:20
 */
class Controller {
    public static $controller_map = array();
    public $css = array();
    public $javascript = array();
    public $db = false;
    public $auth_user = false;
    /**
     * @var Smarty $tpl
     */
    public $tpl = false;
    public $module = '';
    public $seo_title = '';
    public $seo_keywords = '';
    public $seo_description = '';
    public $controller_id = null;
    public $module_id = null;
    public $is_main = false;
    public $title = '';
    public $content = '';
    public $breadcrumb_title = '';
    public $breadcrumb = false;
    public $breadcrumbs = array();
    public $name = '';
    public $page_url = '';
    public $action_url = '';

    /** @var bool $isAjax */
    public $is_ajax = false;

    public function __construct($title = '') {
        $this->title = $title;
        $this->db = core::$db;
        $this->auth_user = core::$auth_user;
        $this->tpl = clone core::$tpl;
    }

    public static function create($controller_data) {
        $module = isset($controller_data['prefix']) ? $controller_data['prefix'] : RunManager::$module;
        $name = $controller_data['controller_name'];
        $controller = RunManager::new_instance($module . '\\controller\\' . $name, $controller_data['spec']);
        $controller->controller_id = $controller_data['id'];
        $controller->module_id = $controller_data['module_id'];
        $controller->name = $controller_data['controller_name'];
        $controller->page_url = $controller_data['page_url'];
        $controller->action_url = $controller_data['action_url'];
        $controller->module = self::$controller_map[$controller_data['controller_name']];
        return $controller;
    }

    public function breadcrumb_title(){
        return '';
    }

    public static function init() {
        $base_path = ROOT . '/default';
        $module_paths = glob($base_path . '/*');
        foreach ($module_paths as $path){
            $module = explode('/', $path);
            $module = end($module);
            $controllers = glob($path . '/controller/*.php');
            foreach ($controllers as $controller) {
                $controller_file = explode('/', $controller);
                $controller_file = end($controller_file);
                list($controller_name, $ext) = explode('.', $controller_file);
                self::$controller_map[$controller_name] = $module;
            }
        }
    }

    public function run() {
        $this->init();
        if (isset($_POST['method'])) {
            $method = $_POST['method'];
            if (!method_exists($this, $method))
                throw new Exception($method . ' отсутствует в ' . get_class($this));

            $args = isset($_POST['args']) ? $_POST['args'] : array();
            $params = array();
            if (is_string($args)) {
                $params = json_decode($args);
                $params = (array)$params;
            }
            if (is_array($args))
                $params = $args;

            try {
                $result = $this->$method($params);
                echo json_encode($result);
                exit;
            } catch (Exception $ex) {
                throw new Exception($ex->getMessage(), 0, $ex);
            }

        }
        else {
            $this->prepare_tpl();
            $this->pre_start();
        }
    }

    /**
     * Заполняет пути для конкретного модуля, где лежат шаблоны
     */
    public function prepare_tpl() {;
        if ($this->module_id != '_common')
            $this->tpl->addTemplateDir(ROOT . '/default/' . $this->module . '/misc/template/');
        $this->tpl->addTemplateDir(ROOT . '/default/_common/misc/template/');
    }

    public function prepare_js() {
        $js_url = array();
        foreach ($this->javascript as $name) {
            $module_path = '/default/' . $this->module . '/misc/js/';
            $common_path = '/default/_common/misc/js/';
            if (file_exists(ROOT . $common_path . $name))
                $js_url[] = BASE_URL . $common_path . $name;
            elseif (file_exists(ROOT . $module_path . $name))
                $js_url[] = BASE_URL . $module_path . $name;;
        }
        return $js_url;
    }

    public function prepare_css() {
        $css_url = array();
        foreach ($this->css as $name) {
            $module_path = '/default/' . $this->module . '/misc/css/';
            $common_path = '/default/_common/misc/css/';
            if (file_exists(ROOT . $common_path . $name))
                $css_url[] = BASE_URL . $common_path . $name;
            elseif (file_exists(ROOT . $module_path . $name))
                $css_url[] = BASE_URL . $module_path . $name;;
        }
        return $css_url;
    }

    public function pre_start() {
        $is_valid = $this->is_valid();

        if ($is_valid) {
            $this->start();
            $seo = $this->seo();

            $this->seo_title = $seo['title'];
            $this->seo_keywords = $seo['keywords'];
            $this->seo_description = $seo['description'];
        } else {
            header("HTTP/1.0 404 Not Found");
            header("HTTP/1.1 404 Not Found");
            header("Status: 404 Not Found");

            $tpl = &$this->tpl;
            $this->seo_title = '404 Not Found';
            $this->seo_keywords = 'неверный адрес ошибка неправильный url битая ссылка';
            $this->seo_description = 'Страница error. Появляется при переходе по битой ссылке, ошибке в адресе или входном параметре.';
            $this->breadcrumb = false;
            $this->content = $tpl->fetch('bad_url.tpl');
        }
    }

    public function seo() {
        $fact = new SeoPageFactory(core::$db);
        $page = $fact->get_list(array('filters' => array(
            'controller_id' => $this->controller_id,
        )))->as_array();

        $data = reset($page);
        return $data;
    }

    public function start(){
        //Переопределять в контроллерах
    }

    /**
     * Проверка POST параметров на валидность
     * @param array $args
     * @return bool
     */
    public function is_valid($args = array()) {
        return true;
    }

    public function global_search($args = array()) {
        $fact = new \adt\model\AdditiveFactory(core::$db);
        $search = isset($args['search']) ? $args['search'] : '';
        $additives = $fact->search_additives($args)->as_array();
        $ul = '';

        if (count($additives) == 0)
            $ul = '<li><span>Нет результатов по вашему запросу</span></li>';

        foreach ($additives as $additive) {
            $value = $additive['code'] . ' ' . $additive['spec'];
            $value = $this->highlight_str($value, $search);
            $url = BASE_URL . '/additive/show/?id=' . $additive['id'];
            $ul .= '<li>
                        <a href="'. $url . '">
                            <span">' . $value .'</span>
                        </a>
                    </li>';
        }
        return $ul;
    }

    /**
     * Подсветить совпадения
     * @param string $haystack
     * @param string $needle
     * @return mixed
     */
    private function highlight_str($haystack = '', $needle = '') {
        $start_pos = stripos(mb_strtolower($haystack), mb_strtolower($needle));
        $str = substr($haystack, $start_pos, strlen($needle));
        $value = str_ireplace($str, '<span class="highlight">' . $str . '</span>', $haystack);
        return $value;
    }

    public function search_additives($args = array()) {
        $fact = new \adt\model\AdditiveFactory(core::$db);
        $additives = $fact->all_additives($args)->as_array();
        $result = array();

        foreach ($additives as $additive) {
            $result[$additive['code'] . ' ' . $additive['spec']] =  null;
        }
        return $result;
    }
}
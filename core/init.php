<?php
use _common\model\map\UserMenuFactory;

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 22.10.16
 * Time: 16:56
 */
class RunManager {
    private $config = array();
    private $_tpl = null;
    private $_auth_user = null;
    public static $module = 'adm';

    /**
     * RunManager constructor.
     * @param array $config
     */
    public function __construct(array $config) {
        $this->config = $config;
        core::$config = $config;
    }

    public function init($module = '_common'){
        self::$module = $module;
        spl_autoload_register('RunManager::load_class');

        $this->_init_db();
        $this->_init_template();
        Controller::init();
    }

    public static function load_class($class_name) {
        if (preg_match('/(.*?)(Factory|Record|View)$/', $class_name, $match))
            $class_name = $match[1];

        $class_parts = explode('\\', $class_name);
        $class = '';
        foreach ($class_parts as $part) {
            $class .= '/' . $part;
        }
        $test = ROOT . '/default' . $class . '.php';
        if (file_exists($test))
            require $test;
    }

    private function _init_db() {
        $db_manager = new DatabaseManager(array(
            'db_site' => $this->config['db_site'],
            'master_db' => $this->config['master_db']
        ));

        /**
         * @var mysql $db | core::$db
         */
        $db = $db_manager->get_connection('db_site');
        $master_db = $db_manager->get_connection('master_db');
        $hostname = isset($this->config['db_site']['dbhostname']) ? $this->config['db_site']['dbhostname'] : 'localhost';
        core::$db_manager = $db_manager;
        core::$db = $db;
        core::$master_db = $master_db;
    }

    private function _init_template() {
        $tpl = new Smarty();
        $tpl->assign('local', BASE_URL);
        $tpl->assign('tpath', TPATH);
        $tpl->assign('protocol', PROTOCOL);
        $tpl->assign('site_name', core::$config['site_name']);
        $tpl->assign('site_email', core::$config['site_email']);
        $tpl->assign('app_url', core::$config['app_url']);

        $local_site = isset(core::$config['local_site']) ? core::$config['local_site'] : false;
        $tpl->assign('local_site', $local_site);
        $this->_tpl = $tpl;
        core::$tpl = $tpl;
    }

    public function get_controller_data() {
        $request_uri = $_SERVER['REQUEST_URI'];
        $request_uri = substr_replace ($request_uri, '', 0, 1);
        $request_uri = explode('/', $request_uri);

        $controller = $this->get_controller_page($request_uri);
        if (empty($controller)) {
            $request_uri = array('not_found');
            $controller = $this->get_controller_page($request_uri);
        }

        $controller = reset($controller);
        return $controller;
    }

    public function get_controller_page($request_uri) {
        $page_url = isset($request_uri[0]) ? $request_uri[0] : '';
        $action_url = isset($request_uri[1]) ? $request_uri[1] : '';

        $fact = new \_common\model\ControllerFactory(core::$db);
        $controller = $fact->get_grid(array('filters' => array(
            'page_url' => $page_url,
            'action_url' => $action_url
        )))->as_array();
        return $controller;
    }

    private function _init_controller() {
        $controller_data = $this->get_controller_data();
        $controller = Controller::create($controller_data);
        return $controller;
    }

    static function new_instance() {
        $args = func_get_args();
        $class_name = array_shift($args);
        if (!class_exists($class_name))
            throw new Exception('Класс не найден!');

        $ref = new ReflectionClass($class_name);
        $obj = $ref->newInstanceArgs($args);
        return $obj;
    }

    public function run(){
        $tpl = core::$tpl;
        $controller = $this->_init_controller();
        $controller->run();
        $navi = $this->build_top_menu();
        $mobile_navi = $this->mobile_menu();

        $tpl->assign('navi', $navi);
        $tpl->assign('mobile_navi', $mobile_navi);
        $tpl->assign('module', $controller->module);
        $tpl->assign('content', $controller->content);
        $tpl->assign('breadcrumb', $controller->breadcrumb);
        $tpl->assign('breadcrumbs', $controller->breadcrumbs);
        $tpl->assign('is_main', $controller->is_main);
        $tpl->assign('seo_title', $controller->seo_title);
        $tpl->assign('seo_keywords', $controller->seo_keywords);
        $tpl->assign('seo_description', $controller->seo_description);
        $tpl->assign('breadcrumb_title', $controller->breadcrumb_title());
        $tpl->assign('javascript', $controller->prepare_js());
        $tpl->assign('css', $controller->prepare_css());
        $tpl->addTemplateDir(ROOT . '/default/_common/misc/template/');
        $tpl->display('main.tpl');
    }

    public function build_top_menu() {
        $fact = new UserMenuFactory(core::$db);
        $menu = $fact->top_menu(array('filters' => array(
            'role_id' => 2,   //TODO эта роль должна браться у auth_user
            'parent_id' => null
        )))->as_array();

        $top_div = '
        <ul id="nav-pc" class="right hide-on-med-and-down">';

        foreach ($menu as $m) {
            $url = $this->get_url($m);
            $top_div .= '<li><a href="' . $url . '" title="' . $m['title'] . '">' . $m['spec'] . '</a></li>';
        }

        $top_div .=
            '</ul>

            <ul class="right">
            <li id="search-icon"><a title="Поиск"><i class="fa fa-search"></i></a></li>
            </ul>
            <form id="hide-form">
                <div class="input-field" id="search-div" style="display: none;">
                    <input type="text" id="autocomplete" placeholder="Поиск по добавкам" autocomplete="off">
                    <label for="autocomplete"></label>
                    <i class="fa fa-times" id="close-icon"></i>
                    <ul class="autocomplete-content dropdown-content" id="search-results"></ul>
                </div>
            </form>';
        return $top_div;
    }

    public function mobile_menu() {
        $fact = new UserMenuFactory(core::$db);
        $menu = $fact->top_menu(array('filters' => array(
            'role_id' => 2,   //TODO эта роль должна браться у auth_user
            'parent_id' => null
        )))->as_array();

        $top_div = '
            <ul id="nav-mobile" class="side-nav" style="transform: translateX(-310px);">';

        foreach ($menu as $m) {
            $url = $this->get_url($m);
            $top_div .= '<li>
                            <a href="' . $url . '">' .
                                '<i class="' . $m['icon'] . '"></i>' . $m['spec']  .
                            '</a>
                        </li>';
        }

        $top_div .=
            '</ul>';
        return $top_div;
    }

    private function get_url($m) {
        $url_arr = array();
        $url_arr[] = BASE_URL;

        if (!empty($m['page_url']))
            $url_arr[] = $m['page_url'];

        if (!empty($m['action_url']))
            $url_arr[] = $m['action_url'];

        $url = implode('/', $url_arr);
        return $url;
    }
}
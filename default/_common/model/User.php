<?php
/**
 * Модель Пользователь
 * Created by PhpStorm.
 * User: denis
 * Date: 20.11.16
 * Time: 11:49
 */
namespace _common\model;


class UserRecord extends \ModelRecord{
    public $id          = 0;    // smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT
    public $spec        = '';   // varchar(192) DEFAULT NULL COMMENT 'ФИО'
    public $login       = '';   // varchar(64) NOT NULL COMMENT 'Логин'
    public $pswd        = '';   // varchar(255) NOT NULL COMMENT 'Пароль'
    public $role_id     = 0;    // tinyint(2) UNSIGNED NOT NULL COMMENT 'Идентификатор роли'
    public $updater_id  = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id  = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time = '';   // date NOT NULL COMMENT 'Дата создания записи'
}

class User extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'adm_user';
        parent::__construct($db, $data);
    }
}

class UserFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'adm_user';
        parent::__construct($db);
    }
}
<?php
/**
 * Модель Роль
 * Created by PhpStorm.
 * User: denis
 * Date: 20.11.16
 * Time: 13:15
 */
namespace _common\model;

class RoleRecord extends \ModelRecord{
    public $id          = 0;    // tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENTT
    public $spec        = '';   // varchar(32) DEFAULT NULL COMMENT 'Наименование'
    public $comment     = '';   // text DEFAULT NULL COMMENT 'Описание'
}

class Role extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_role';
        parent::__construct($db, $data);
    }
}

class RoleFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_role';
        parent::__construct($db);
    }
}
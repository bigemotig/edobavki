<?php
namespace _common\model;
/**
 * Модель описывающая таблицу контроллеров
 * Created by PhpStorm.
 * User: denis
 * Date: 31.10.16
 * Time: 21:01
 */
class ControllerRecord extends \ModelRecord{
    public $id  = 0;                // smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT
    public $controller_name = '';   // varchar(32) NOT NULL COMMENT 'Имя контроллера'
    public $spec            = '';   // varchar(64) NOT NULL COMMENT 'Название'
    public $page_url        = '';   // varchar(255) DEFAULT ''
    public $action_url      = '';   // varchar(255) DEFAULT ''
    public $module_id       = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор модуля'
    public $no_index        = 0;    // tinyint(1) UNSIGNED DEFAULT 0 COMMENT 'Будет ли индексироваться поисковиками'
    public $icon            = '';   // varchar(32) DEFAULT NULL COMMENT 'Иконка'
    public $priority        = null; // decimal(2, 1) DEFAULT NULL COMMENT 'Приоритет'
}

class ControllerView extends \ModelView{

}

class Controller extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_controller';
        parent::__construct($db, $data);
    }
}

class ControllerFactory extends \Factory{

    public function __construct($db) {
        $this->table = 'sys_controller';
        $this->default_order_field = 'controller_name';
        parent::__construct($db);
    }

    public function get_grid($args = array()) {
        $sql = new \SqlWriter($this->table);

        $sql
            ->set_field(array('id', 'controller_name', 'spec', 'module_id', 'page_url', 'action_url'))
            ->set_extra_field(array('table' => 'sys_module', 'field' => 'prefix'))
            ->set_join(array('type' => 'LEFT', 'join_table' => 'sys_module', 'join_field' => 'id', 'target_field' => 'module_id'));

        $args['sql'] = $sql;
        return parent::get_list($args);
    }
}
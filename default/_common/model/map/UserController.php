<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 28.11.16
 * Time: 14:37
 */

namespace _common\model\map;

class UserControllerRecord extends \ModelRecord{
    public $id              = 0;    // int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор''
    public $user_id         = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор пользователя'
    public $controller_id   = 0;    // controller_id smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор контроллера'
    public $updater_id      = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time     = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id      = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time     = '';   // date NOT NULL COMMENT 'Дата создания запи
}

class UserController extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_user_controller';
        parent::__construct($db, $data);
    }
}

class UserControllerFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_user_controller';
        parent::__construct($db);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 14.02.17
 * Time: 11:29
 */
namespace _common\model\map;

class SeoPageRecord extends \ModelRecord{
    public $id              = 0;    // int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'
    public $controller_id   = 0;    // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор контроллера'
    public $post_id         = 0;    // int(11) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор из POST запроса'
    public $title           = '';   // varchar(255) NOT NULL COMMENT 'Заголовок'
    public $keywords        = '';   // varchar(255) DEFAULT NULL COMMENT 'Ключевые слова для страницы'
    public $description     = '';   // varchar(255) DEFAULT NULL COMMENT 'Краткое описание страницы'
    public $is_active       = 1;    // tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Активный ли URL'
    public $for_sitemap     = 1;    // tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Использовать при формировании sitemap'
    public $updater_id      = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time     = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id      = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time     = '';   // date NOT NULL COMMENT 'Дата создания запи
}

class SeoPage extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_seo_page';
        parent::__construct($db, $data);
    }
}

class SeoPageFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_seo_page';
        parent::__construct($db);
    }

    public function top_pages($args = array()) {
        $sql = new \SqlWriter($this->table);
        $sql
            ->set_field(array('id', 'controller_id', 'update_time', 'create_time'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'priority'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'page_url'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'action_url'))
            ->set_join(array('join_table' => 'sys_controller', 'join_field' => 'id', 'target_field' => 'controller_id'))
            ->set_order(array('alias' => 'none', 'field' => 'priority', 'sort' => 'DESC'))
        ;
        $args['sql'] = $sql;
        return parent::get_list($args);
    }

    public function additive_pages($args = array()) {
        $sql = new \SqlWriter($this->table);
        $sql
            ->set_field(array('id', 'controller_id', 'post_id', 'update_time', 'create_time'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'priority'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'page_url'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'action_url'))
            ->set_join(array('join_table' => 'sys_controller', 'join_field' => 'id', 'target_field' => 'controller_id'))
            ->set_join(array('join_table' => 'adt_additive', 'join_field' => 'id', 'target_field' => 'post_id'))
            ->set_where(array('table' => $this->table, 'field' => 'controller_id', 'value' => 10))
            ->set_order(array('alias' => 'none', 'field' => 'priority', 'sort' => 'DESC'))
        ;
        $args['sql'] = $sql;
        return parent::get_list($args);
    }
}
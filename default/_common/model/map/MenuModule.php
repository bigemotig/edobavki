<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 28.11.16
 * Time: 14:37
 */

namespace _common\model\map;


class MenuModuleRecord extends \ModelRecord{
    public $id          = 0;    // UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'
    public $module_id   = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор модуля'
    public $menu_id     = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор меню'
    public $start_page  = 0;    // tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Стартовая страница?'
    public $updater_id  = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id  = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time = '';   // date NOT NULL COMMENT 'Дата создания запи
}

class MenuModule extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_menu_module';
        parent::__construct($db, $data);
    }
}

class MenuModuleFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_menu_module';
        parent::__construct($db);
    }
}
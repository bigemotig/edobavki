<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 28.11.16
 * Time: 14:37
 */

namespace _common\model\map;

class UserMenuRecord extends \ModelRecord{
    public $id              = 0;    // int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'
    public $seq             = 0;    // smallint(5) UNSIGNED DEFAULT 0 COMMENT 'Организовывает порядок запсей'
    public $parent_id       = null; // parent_id int(11) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор родительского пункта'
    public $role_id         = 0;    // role_id tinyint(2) UNSIGNED NOT NULL COMMENT 'Идентификатор роли'
    public $controller_id   = 0;    // controller_id smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор контроллера'
    public $spec            = '';   // spec varchar(64) NOT NULL COMMENT 'Наименование'
    public $updater_id      = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time     = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id      = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time     = '';   // date NOT NULL COMMENT 'Дата создания запи
}

class UserMenu extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_user_menu';
        parent::__construct($db, $data);
    }
}

class UserMenuFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_user_menu';
        parent::__construct($db);
    }

    public function top_menu($args = array()) {
        $sql = new \SqlWriter($this->table);

        $sql
            ->set_field(array('id', 'spec'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'page_url'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'action_url'))
            ->set_extra_field(array('table' => 'sys_controller', 'field' => 'icon'))
            ->set_extra_field(array('table' => 'sys_seo_page', 'field' => 'title'))
            ->set_join(array('join_table' => 'sys_controller', 'join_field' => 'id', 'target_field' => 'controller_id'))
            ->set_join(array('join_table' => 'sys_seo_page', 'join_field' => 'controller_id', 'target_field' => 'controller_id'))
            ->set_order(array('table' => $this->table, 'field' => 'seq', 'sort' => 'ASC'))
        ;

        $args['sql'] = $sql;
        return parent::get_list($args);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.11.16
 * Time: 22:13
 */
namespace _common\model;


class MenuRecord extends \ModelRecord{
    public $id              = 0;    // smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'NT
    public $controller_id   = '';   // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор контроллера'
    public $parent_id       = '';   // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Родительское меню''
    public $spec            = '';   // varchar(32) NOT NULL COMMENT 'Наименование'
    public $updater_id      = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time     = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id      = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time     = '';   // date NOT NULL COMMENT 'Дата создания записи'
}

class Menu extends \Model{
    public function __construct($db, $data = null) {
        $this->table = 'sys_menu';
        parent::__construct($db, $data);
    }
}

class MenuFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_menu';
        parent::__construct($db);
    }
}
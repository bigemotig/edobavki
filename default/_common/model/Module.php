<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.11.16
 * Time: 22:13
 */
namespace _common\model;


class ModuleRecord extends \ModelRecord{
    public $id          = 0;    // smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT
    public $spec        = '';   // varchar(32) NOT NULL COMMENT 'Название модуля'
    public $prefix      = '';   // varchar(4) NOT NULL COMMENT 'Префикс модуля'
}

class Module  extends \Model{
    const COMMON    = 1;
    const ADM       = 2;
    const KPI       = 3;
    public function __construct($db, $data = null) {
        $this->table = 'sys_module';
        parent::__construct($db, $data);
    }
}

class ModuleFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'sys_module';
        parent::__construct($db);
    }
}
<div class="section" id="start_page">
    <div class="row">
        <div class="col s12 offset-m2 m8 l6 center-align" id="app_block">
            <h2 class="center" style="margin-bottom: 2rem">Приложение для смартфонов</h2>
            <div>
                <img class="responsive-img" src="{$tpath}image/edobavki-preview.png" alt="Поиск в Android приложении ЕДобавки">
            </div>
            <a class="waves-effect waves-light btn col s12 offset-m2 m8 offset-l4 l4" href="{$local}/mobile_application/" id="about_app" title="Мобильное приложение">Подробнее</a>
        </div>

        <div class="col s12 offset-m1 m10 l6" id="news_block">
            <div class="row">
                <h2 class="title center">Новости проекта</h2>
            </div>
            {foreach from=$last_news item=$news}
                <div class="col s12 m12">
                    <div class="row">
                        <h3 class="left">({$news.create_time}) {$news.spec}</h3>
                    </div>
                    <div class="row">
                        <p class="light">
                            {$news.announcement}
                        </p>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>
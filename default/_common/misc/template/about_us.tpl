<div class="section" id="about_us">
    <div class="row">
        <div class="col s12 offset-m3 m6">
            <h2 class="start-feature-heading" style="margin-top: 10px;">Сообщить об ошибке</h2>
            <p class="text-darken-1 start-paragraph-text">
                Спасибо, что Вы посетили наш интернет-справочник <b>«{$site_name}»</b>.
                Мы постоянно работаем над улучшением нашего продукта,
                поэтому если Вы заметили опечатку или неверную информацию,
                Вы можете сообщить об этом на наш E-mail
                {$site_email} указав в письме:
            </p>

            <ul style="margin-top: 10px;">
                <li>1. код добавки (пример: Е100);</li>
                <li>2. описать, где находится неточность, опечатка, устаревная информация и т.д;</li>
                <li>3. приложить ссылку на авторитетный источник с верной информацией.</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col s12 offset-m3 m6">
            <h2 class="start-feature-heading" style="margin-top: 10px;">При создании сайта использовались</h2>
            <ul>
                <li>
                    1. Сарафанова Л.А. Пищевые добавки: Энциклопедия. — 2-е изд.,
                    испр. и доп. — СПб: ГИОРД, 2004. — 808 с.
                </li>
                <li>
                    2. Требования безопасности пищевых добавок, ароматизаторов
                    и технологических вспомогательных средств от 20 июля 2012 г. № 58,
                    <a href="http://www.eurasiancommission.org/ru/Lists/EECDocs/RS_P_58.pdf">
                        (Ссылка).
                    </a>
                </li>
                <li>
                    3. Current EU approved additives and their E Numbers,
                    <a href="https://www.food.gov.uk/science/additives/enumberlist">
                        (Ссылка).
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col s12 offset-m3 m6">
            <h2 class="start-feature-heading" style="margin-top: 10px;">Использование информации</h2>
            <p class="text-darken-1 start-paragraph-text" style="margin-top: 10px;">
                Информация на сайте<b> «{$site_name}»</b> предоставляется в ознакомительных целях и разработчики приложения не несут за нее ответственности.
                При размещении информации на другом сайте указывайте ссылку на наш ресурс.
            </p>
        </div>
    </div>
</div>
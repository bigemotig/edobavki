<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="ru-RU">

<head>
    <title>{$seo_title}</title>
    <meta charset="UTF-8" />
    <meta name="keywords" content="{$seo_keywords}">
    <meta name="description" content="{$seo_description}">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    {if $is_main}
        <meta name="yandex-verification" content="ff7a103be0fa6b86" />
        <meta name="google-site-verification" content="fwrPIyyZ0NdPRLVN1j2lhH2PlVTevJAKVyK1pNTSGlQ" />
    {/if}
    <link rel="shortcut icon" href="{$tpath}image/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{$tpath}css/font-awesome.min.css" media="all" />
    <link type="text/css" rel="stylesheet" href="{$tpath}css/materialize.min.css"  media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{$tpath}css/materialize-theme.css"  media="screen,projection" />
    <link rel="stylesheet" type="text/css" href="{$tpath}css/style.css" media="all" />

    {if $css}
        {foreach from=$css item=src}
            <link rel="stylesheet" type="text/css" href="{$src}" media="all" />
        {/foreach}
    {/if}

    {if $local_site == false}
        <!-- Google START -->
        {literal}
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-93153629-1', 'auto');
                ga('send', 'pageview');

            </script>
        {/literal}
        <!-- Google END -->

        <!-- Yandex START -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter43276364 = new Ya.Metrika({
                            id:43276364,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/43276364" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- Yandex END -->
    {/if}
</head>

<body>
<script type="text/javascript" src="{$tpath}js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="{$tpath}js/common.js"></script>
<script type="text/javascript" src="{$tpath}js/materialize.min.js"></script>

{if $javascript}
    {foreach from=$javascript item=src}
        <script type="text/javascript" src="{$src}"></script>
    {/foreach}
{/if}

<header>
    <nav>
        <div class="nav-wrapper container">
            <a href="#" data-activates="nav-mobile" class="button-collapse hide-on-large-only" id="hamburger"><i class="fa fa-bars"></i></a>
            <a class="brand-logo" href="{$local}" title="Пищевые добавки и их влияние на организм человека">{$site_name}</a>
            {$navi}
            {$mobile_navi}
        </div>
    </nav>
</header>

{if $breadcrumb}
    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <h1>{$breadcrumb_title}</h1>
                <nav id="breadcrumbs">
                    <div class="nav-wrapper">
                        <div class="col s12" style="text-align: center;">
                            {if $is_main}

                            {else}
                                <a href="{$local}" class="breadcrumb" title="Пищевые добавки и их влияние на организм человека">Главная</a>
                            {/if}

                            {foreach from=$breadcrumbs item=br}
                                <a {if $br.href}href="{$local}{$br.href}" title="{$br.title}" {/if} class="breadcrumb">{$br.spec}</a>
                            {/foreach}
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="parallax">
            <img src="{$tpath}image/breadcrumbs_background.jpg" style="display: block; transform: translate3d(-50%, 342px, 0px);" alt="фон хлебных крошек">
        </div>
    </div>
{/if}

<div id="wrapper" class="container">
    {$content}
</div>

<footer class="page-footer center white-text teal">
    <div class="container">
        <div class="row">
            <a class="white-text" href="{$local}/about_us" title="Информация о проекте ЕДобавки">О сайте</a>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Разработал krad
        </div>
    </div>
</footer>
</body>
</html>


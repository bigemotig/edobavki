<div class="section" id="bad_url">
    <div class="row">
        <div class="col s12 m12 center">
            <h1 class="center">Упс! Страница не найдена</h1>
        </div>
        <div class="col s12 m6 center-align">
            <div>
                <img class="responsive-img" src="{$tpath}image/error.png" alt="Сообщение об ошибке">
            </div>
        </div>

        <div class="col s12 m6">
            <div class="row col s12 m7">
                <h3 class="center">Советы</h3>
                <ul>
                    <li>попробуйте вернуться назад;</li>
                    <li>перейдите на <a href="{$local}" title="Пищевые добавки и их влияние на организм человека">главную</a>;</li>
                    <li>воскользуйтесь поиском, который находится сверху;</li>
                    <li>установите наше <a href="{$local}/mobile_application/" title="Мобильное приложение">мобильное приложение</a> (в нем нет проблем с битыми ссылками).</li>
                </ul>
            </div>
        </div>
    </div>
</div>
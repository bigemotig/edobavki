<div class="section" id="mobile_app">
    <div class="row">
        <div class="col s12 m12" style="text-align: center">
            <h2 class="title">Бесплатный справочник</h2>
        </div>

        <div class="col s12  offset-m2 m8 l6">
            <img class="responsive-img" src="{$tpath}image/prilozhenie-edobavki.png" alt="ЕДобавки android приложение">
        </div>

        <div class="col s12 m12 l6">
            <h3 class="center">О программе</h3>

            <p>
                Предлагаем Вам попробовать наше бесплатное мобильное приложение <b>{$site_name}</b> для операционной системы
                Android. От аналогов его отличает свежий материальный дизайн, огромная база пищевых добавок (на данный момент {$additive_count}),
                а так же удобное управление, основанное на рекомендациях создателей операционной системы. Данные будут находиться прямо на
                Вашем смартфоне, что позволит узнать всю нужную информацию без интернета.
            </p>

            <a class="waves-effect waves-light btn col s12  offset-m2 m8 offset-l3 l6" href="{$app_url}" id="download">Установить</a>
        </div>
    </div>

    <div class="row center" id="screenshots">
        <div class="col s12 m12">
            <h2 class="title center">Скриншоты</h2>
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="{$tpath}image/edobavki-spisok.jpg" alt="Список пищевых добавок с их кодами">
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="{$tpath}image/edobavki-kartochka.jpg" alt="Карточка пищевой добавки">
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="{$tpath}image/edobavki-menyu.jpg" alt="Левое выдвигающиеся меню приложения">
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="{$tpath}image/edobavki-nastrojki.jpg" alt="Настроки мобильного справочника ЕДобавки">
        </div>
    </div>

    <div class="row" id="cards_block">
        <div class="col s12 m12">
            <h2 class="title center">Почему именно наше приложение?</h2>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-smile-o fa-2x"></i>
                    <span class="card-title">Дизайн</span>
                    <p class="bounceEffect">
                        Приложение выполнено в материальном дизайне.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-rocket fa-2x"></i>
                    <span class="card-title">Удобство</span>
                    <p class="bounceEffect">
                        Быстрый поиск и удобная навигация.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-signal fa-2x"></i>
                    <span class="card-title">Сеть</span>
                    <p class="bounceEffect">
                        Приложению не нужен для работы интернет.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-pie-chart fa-2x"></i>
                    <span class="card-title">Поддержка</span>
                    <p class="bounceEffect">
                        Запустится на 99% Android устройств.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m12" style="height: 5em">
            <a class="waves-effect waves-light btn col s12  offset-m2 m8 offset-l4 l4" href="{$app_url}" id="download">Установить</a>
        </div>
    </div>
</div>
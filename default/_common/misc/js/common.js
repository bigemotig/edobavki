/**
 * Created by denis on 20.11.16.
 */
$(document).ready(function () {
    var common = new Common();
    common.init();
});

function Common() {
    this.search_results = $('#search-results');
    this.autocomplete = $('#autocomplete');
}

Common.prototype = {
    init: function() {
        var $this = this;
        $('.button-collapse').sideNav();
        $('.parallax').parallax();
        $('input#input_text').characterCounter();

        $this.init_search();
        $this.disable_enter();
    },
    init_search: function () {
        var $this = this;
        $($this.autocomplete).val("");

        $('#search-icon').on('click', function () {
            $('#search-icon').hide();
            $('#hamburger').hide();
            $('#nav-pc').hide();
            $('#search-div').fadeIn();
            $($this.autocomplete).focus();
            return false;
        });

        $('#close-icon').on('click', function () {
            $('#search-div').hide();
            $('#search-icon').fadeIn();
            $('#nav-pc').fadeIn();
            $('#hamburger').fadeIn();
            return false;
        });

        $($this.autocomplete).keyup(function() {
            var num_chars = $(this).val().length;

            if (num_chars >= 3) {
                var args = {};
                args.search = $(this).val();
                $this.global_search(args);
            } else {
                $($this.search_results).empty();
            }
        });
    },

    global_search: function (args) {
        var $this = this;
        ajax_request({
            method: 'global_search',
            args: args,
            callback: function(response) {
                $($this.search_results).html(response);
            }
        });
    },

    disable_enter: function () {
        $('#autocomplete').keypress(function(e){
            if ( e.which == 13 ) return false;
        });
    }
};

function ajax_request(args){
    var url = document.location;
    if (args.url != undefined && args.url.length > 0)
        url = args.url;

    var _args = {};
    if (args.args != undefined)
        _args = args.args;

    var data = {
        method : args.method,
        args: JSON.stringify(_args)
    };

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: url,
        data: data,
        error: function () {

        },
        success: function(response){
            if (args.callback !== undefined && args.callback instanceof Function)
                args.callback(response);
        }
    });
}

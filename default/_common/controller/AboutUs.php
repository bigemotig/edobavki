<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 12.02.17
 * Time: 13:59
 */
namespace _common\controller;

class AboutUs extends \Controller {
    public function start() {
        $tpl = &$this->tpl;
        $this->css = array(
            'about_us.css'
        );
        $this->breadcrumb = true;
        $this->breadcrumbs = $this->breadcrumbs();
        $this->content = $tpl->fetch('about_us.tpl');
    }

    public function breadcrumb_title() {
        return 'Информация о сайте';
    }

    public function breadcrumbs($args = array()) {
        $breadcrumbs = array(
//            array('spec' => 'О сайте', 'href' => false)
        );
        return $breadcrumbs;
    }
}
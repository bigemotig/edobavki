<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.02.17
 * Time: 16:29
 */

namespace _common\controller;


class NotFound404 extends \Controller {
    public function start() {
        header("HTTP/1.0 404 Not Found");
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");

        $tpl = &$this->tpl;
        $this->css = array(
            'not_found404.css'
        );

        $this->content = $tpl->fetch('bad_url.tpl');
    }
}
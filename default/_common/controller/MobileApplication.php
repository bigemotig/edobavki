<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.02.17
 * Time: 11:39
 */
namespace _common\controller;

use adt\model\AdditiveFactory;

class MobileApplication extends \Controller {
    public function start() {
        $tpl = &$this->tpl;
        $this->css = array(
           'mobile_application.css'
        );
        $this->javascript = array(
            'mobile_application.js'
        );
        $additive_count = $this->additive_count();

        $this->breadcrumb = true;
        $this->breadcrumbs = $this->breadcrumbs();
        $this->tpl->assign('additive_count', $additive_count);
        $this->content = $tpl->fetch('mobile_application.tpl');
    }

    public function breadcrumb_title() {
        return 'Мобильное приложение';
    }

    public function breadcrumbs($args = array()) {
        $breadcrumbs = array(
//            array('spec' => 'Мобильное приложение', 'href' => false)
        );
        return $breadcrumbs;
    }

    public function additive_count() {
        $fact = new AdditiveFactory(\core::$db);
        $additives = $fact->get_list()->as_array();
        $count = count($additives);
        return $count;
    }
}
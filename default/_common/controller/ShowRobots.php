<?php
/**
 * Контроллер показа robots.txt
 * Created by PhpStorm.
 * User: denis
 * Date: 15.02.17
 * Time: 13:54
 */
namespace _common\controller;


class ShowRobots extends \Controller{

    public function start() {
        header('Content-Type: text/plain');
        $myFile = "robots.txt";

        $fh = fopen($myFile, 'r');
        $theData = fread($fh, filesize($myFile));
        fclose($fh);

        echo $theData;
        die();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.02.17
 * Time: 12:42
 */
namespace _common\controller;

class LoginAs extends \Controller {
    public function start() {
        $tpl = &$this->tpl;
        $this->content = $tpl->fetch('login_as.tpl');
    }
}
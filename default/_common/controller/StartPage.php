<?php
namespace _common\controller;
use nws\model\NewsFactory;

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 23.10.16
 * Time: 12:39
 */
class StartPage extends \Controller{
    public function start() {
        $tpl = &$this->tpl;
        $this->breadcrumb = true;
        $this->is_main = true;
        $this->css = array(
            'start_page.css'
        );
        $last_news = $this->last_news();

        $this->tpl->assign('last_news', $last_news);
        $this->content = $tpl->fetch('start_page.tpl');
    }

    public function breadcrumb_title() {
        return 'Справочник пищевых добавок';
    }

    public function last_news() {
        $fact = new NewsFactory(\core::$db);
        $news = $fact->last_news()->as_array();
        return $news;
    }
}
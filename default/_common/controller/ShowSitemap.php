<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 15.02.17
 * Time: 14:29
 */
namespace _common\controller;

class ShowSitemap extends \Controller {

    public function start() {
        header('Content-Type: text/xml');
        $myFile = "sitemap.xml";

        $fh = fopen($myFile, 'r');
        $theData = fread($fh, filesize($myFile));
        fclose($fh);

        echo $theData;
        die();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 15.02.17
 * Time: 18:58
 */

namespace adm\controller;
use _common\model\map\SeoPageFactory;

class SitemapGenerate extends \Controller {
    public function start() {
        $pages = $this->get_pages();
        $sitemap_body = '';

        foreach ($pages as $page)
            $sitemap_body .= $this->get_url($page);

        $content =
                    '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.
                        $sitemap_body.
                   '</urlset>';

        try {
            $handle = fopen('sitemap.xml', 'w+');
            fwrite($handle, $content);
            echo 'Файл sitemap обновлен!';
        }
        catch (\Exception $ex) {
          echo 'Во время обновления произошла ошибка. Файл sitemap не обновлен!';
        }
        die();
    }

    private function get_pages() {
        $fact = new SeoPageFactory(\core::$db);
        $root_pages = $fact->top_pages(array('filters' => array(
            'post_id' => null,
            'is_active' => 1,
            'for_sitemap' => 1
        )))->as_array();

        $additive_pages = $fact->additive_pages(array('filters' => array(
            'is_active' => 1,
            'for_sitemap' => 1
        )))->as_array();

        $pages = array_merge($root_pages, $additive_pages);
        return $pages;
    }

    private function get_url($page) {
        $lastmod = isset($page['update_time']) ? $page['update_time'] : $page['create_time'];
        $location = $this->create_location($page);
        if (!empty($page['post_id']))
            $location .= '?id=' . $page['post_id'];
        $url =
            '<url>
                <loc>' . $location . '</loc>
                <lastmod>' . $lastmod . '</lastmod>
                <changefreq>' . 'weekly' . '</changefreq>
                <priority>' . $page['priority'] . '</priority>
            </url>';
        return $url;
    }

    private function create_location($page) {
        $url_arr = array();
        $url_arr[] = BASE_URL;
        if (!empty($page['page_url']))
            $url_arr[] = $page['page_url'];
        if (!empty($page['action_url']))
            $url_arr[] = $page['action_url'];
        $location = implode('/' , $url_arr);
        return $location . '/';
    }
}
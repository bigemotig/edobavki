<div class="section" id="additive_list">
    <div class="row">
        <div class="col s12 offset-m2 m8">
            <ul class="collapsible popout" data-collapsible="accordion">
                {foreach from=$additive_list item=colapsible}
                    <li>
                        <div class="collapsible-header" id="additives">{$colapsible.spec} (E{$colapsible.min} — E{$colapsible.max})</div>
                        <div class="collapsible-body">
                            {if $colapsible.additives}
                                {foreach from=$colapsible.additives item=additive}
                                    <a href="{$local}/additive/show/?id={$additive.id}" title="{$additive.title}">
                                        <div class="row additive">
                                            <div class="col s1 m1">{$additive.code}</div>
                                            <div class="col s10 m10" style="margin-left: 20px;">{$additive.spec}</div>
                                        </div>
                                    </a>
                                {/foreach}
                            {/if}
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
</div>

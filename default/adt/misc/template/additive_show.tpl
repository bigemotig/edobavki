<div class="section" id="additive_show">
    <div class="row">
        <div class="col s12 m12 offset-l2 l8">
            <h2 class="start-feature-heading">Название</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.spec}</p>

            <h2 class="start text-primarycolor">Технологические функции</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.functions}</p>

            <h2 class="start text-primarycolor">Внешний вид</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.appearance}</p>

            <h2 class="start text-primarycolor">Токсичность</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.negative_effects}</p>

            <h2 class="start text-primarycolor">Природный источник</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.extracted_from}</p>

            <h2 class="start text-primarycolor">Евразийский экономический союз</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.in_russia}</p>

            <h2 class="start text-primarycolor">Европейский союз</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{$additive.in_eu}</p>

            <h2 class="start text-primarycolor">Применение</h2>
            <p class="grey-text m6 text-darken-1 start-paragraph-text">{$additive.employment}</p>
        </div>

        <div class="col s12 m12 offset-l2 l8">
            {if isset($additive.prev_id)}
                <a class="waves-effect keyboard arrow left btn" href="{$local}/additive/show/?id={$additive.prev_id}" title="{$additive.prev_title}">
                    <i class="fa fa-chevron-left fa-1"></i><span> {$additive.prev_code}</span>
                </a>
            {/if}

            {if isset($additive.next_id)}
                <a class="waves-effect waves-light btn right" href="{$local}/additive/show/?id={$additive.next_id}" title="{$additive.next_title}">
                    <span>{$additive.next_code} </span><i class="fa fa-chevron-right"></i>
                </a>
            {/if}
        </div>
    </div>
</div>
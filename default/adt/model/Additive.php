<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.11.16
 * Time: 22:13
 */
namespace adt\model;


class AdditiveRecord extends \ModelRecord{
    public $id                  = 0;    // smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'
    public $code                = '';   // varchar(16) NOT NULL COMMENT 'Код добавки'
    public $spec                = '';   // text NOT NULL COMMENT 'Наименование'
    public $functions           = '';   // text DEFAULT NULL COMMENT 'Технологические функции'
    public $in_russia           = 0;    // tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Разрешена ли в таможенном союзе?http://webportalsrv.gost.ru/portal/GostNews.nsf/acaf7051ec840948c22571290059c78f/9fe752e7e38cc18e44257bde0024e7d4/$FILE/TR_TS_029-2012_text.pdf'
    public $appearance          = '';   // text DEFAULT NULL COMMENT 'Внешний вид добавки'
    public $in_eu               = 0;    // tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Разрешена ли в Евросоюзе? https://www.food.gov.uk/science/additives/enumberlist'
    public $negative_effects    = '';   // text DEFAULT NULL COMMENT 'Отрицательное'
    public $seq                 = 0;    // smallint(5) DEFAULT 0 COMMENT 'Число из кода добавки'
    public $order_number        = 0;    // smallint(5) DEFAULT 0 COMMENT 'Число для сортировки если у добавки есть a b c d.. поддобавки'
    public $information         = '';   // text NOT NULL COMMENT 'Информация о добавке',
    public $employment          = '';   // text NOT NULL COMMENT 'Использование добавки'
    public $extracted_from      = '';   // text NOT NULL COMMENT 'Получают из..'
    public $updater_id          = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time         = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id          = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time         = '';   // date NOT NULL COMMENT 'Дата создания запи
}

class Additive extends \Model{
    const ALLOWED       = 1;    // разрешена
    const DISALLOWED    = 0;    // запрещена

    const IN_RU_KEY     = 'in_russia';
    const IN_EU_KEY     = 'in_eu';

    public function __construct($db, $data = null) {
        $this->table = 'adt_additive';
        parent::__construct($db, $data);
    }
}

class AdditiveFactory extends \Factory{
    public function __construct($db) {
        $this->table = 'adt_additive';
        parent::__construct($db);
    }

    public function all_additives($args = array()) {
        $sql = new \SqlWriter($this->table);
        $sql
            ->set_field(array('id', 'code', 'spec', 'seq', 'order_number'))
            ->set_extra_field(array('table' => 'sys_seo_page', 'field' => 'title'))
            ->set_join(array('join_table' => 'sys_seo_page', 'join_field' => 'post_id', 'target_field' => 'id'))
            ->set_where(array('table' => 'sys_seo_page', 'field' => 'controller_id', 'value' => 10))
            ->set_orders(array('seq' => 'ASC', 'order_number' => 'ASC'))
        ;
        $args['sql'] = $sql;
        return parent::get_list($args);
    }

    public function search_additives($args = array()) {
        $search = isset($args['search']) ? $args['search'] : '';
        $sql = new \SqlWriter($this->table);
        $sql
            ->set_field(array('id', 'code', 'spec'))
            ->set_where(array(
                'connector' => 'OR',
                array('table' => $this->table, 'field' => 'spec', 'compare' => 'LIKE', 'value' => '%'.$search.'%' ),
                array('table' => $this->table, 'field' => 'code', 'compare' => 'LIKE', 'value' => '%'.$search.'%' )
            ))
            ->set_orders(array('seq' => 'ASC', 'order_number' => 'ASC'))
            ->set_limit(10)
        ;

        $args['sql'] = $sql;
        return parent::get_list($args);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.02.17
 * Time: 11:11
 */
namespace adt\controller;

use adt\model\AdditiveFactory;

class AdditiveList extends \Controller {
    public function start() {
        $tpl = &$this->tpl;
        $this->breadcrumb = true;
        $this->css = array(
            'additive_list.css'
        );
        $this->javascript = array(
            'additive_list.js'
        );
        $additive_list = $this->additive_list();

        $this->tpl->assign('additive_list', $additive_list);
        $this->breadcrumbs = $this->breadcrumbs();
        $this->content = $tpl->fetch('additive_list.tpl');
    }

    public function collapse_list($args = array()) {
        $list = array(
            array('spec' => 'Красители', 'min' => 100, 'max' => 199),
            array('spec' => 'Консерванты', 'min' => 200, 'max' => 299),
            array('spec' => 'Антиокислители', 'min' => 300, 'max' => 399),
            array('spec' => 'Стабилизаторы, загустители, эмульгаторы', 'min' => 400, 'max' => 499),
            array('spec' => 'Регуляторы кислотности и вещества против слёживания', 'min' => 500, 'max' => 599),
            array('spec' => 'Усилители вкуса и аромата, ароматизаторы', 'min' => 600, 'max' => 699),
//            array('spec' => 'Антибиотики', 'min' => 700, 'max' => 799),
            array('spec' => 'Прочие ', 'min' => 900, 'max' => 999),
            array('spec' => 'Дополнительные вещества ', 'min' => 1000, 'max' => 1999)
        );
        return $list;
    }

    public function additive_list($args = array()) {
        $collapse_list = $this->collapse_list();
        $fact = new AdditiveFactory(\core::$db);
        $additives = $fact->all_additives()->as_array();

        foreach ($collapse_list as &$value) {
            foreach ($additives as $add) {
                if ($add['seq'] >= $value['min'] && $add['seq'] <= $value['max'])
                    $value['additives'][] = $add;
            }
        }
        return $collapse_list;
    }

    public function breadcrumb_title() {
        return 'Пищевые добавки';
    }

    public function breadcrumbs($args = array()) {
        $breadcrumbs = array(
//            array('spec' => 'Добавки', 'href' => false)
        );
        return $breadcrumbs;
    }
}
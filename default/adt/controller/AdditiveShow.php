<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.02.17
 * Time: 11:11
 */
namespace adt\controller;

use _common\model\map\SeoPageFactory;
use adt\model\Additive;
use adt\model\AdditiveFactory;
use core;

class AdditiveShow extends \Controller {
    private $id;

    public function is_valid() {
        $id = isset($_GET['id']) ? $_GET['id'] : 0;

        $fact = new AdditiveFactory(\core::$db);
        $additive_ids = $fact->get_list()->as_ids();
        $is_valid = in_array($id, $additive_ids);

        return $is_valid;
    }

    public function start() {
        $tpl = &$this->tpl;
        $this->breadcrumb = true;

        $this->css = array(
            'additive_show.css'
        );
        $this->id = isset($_GET['id']) ? $_GET['id'] : 0;

        $additive = $this->additive();
        $this->tpl->assign('additive', $additive);
        $this->breadcrumbs = $this->breadcrumbs($additive);
        $this->content = $tpl->fetch('additive_show.tpl');
    }

    public function breadcrumbs($args = array()) {
        $breadcrumbs = array(
            array('spec' => 'Пищевые добавки', 'href' => '/additive/list', 'title' => 'Пищевые добавки'),
//            array('spec' => $args['code'], 'href' => false)
        );
        return $breadcrumbs;
    }

    public function seo() {
        $id = isset($_GET['id']) ? $_GET['id'] : 0;

        $fact = new SeoPageFactory(core::$db);
        $page = $fact->get_list(array('filters' => array(
            'controller_id' => $this->controller_id,
            'post_id' => $id
        )))->as_array();
        $data = reset($page);
        return $data;
    }

    public function breadcrumb_title() {
        $fact = new AdditiveFactory(\core::$db);
        $additive = $fact->get($this->id);
        return $additive['code'] .' пищевая добавка';
    }

    public function additive($args = array()) {
        $fact = new AdditiveFactory(\core::$db);
        $additive = $fact->get($this->id);

        $additives = $this->additives()->as_array();
        $count = count($additives);

        for ($i = 0; $i < $count; $i++) {
            if ($additives[$i]['id'] == $this->id) {
                if ($i != 0) {
                    $additive['prev_id'] = $additives[$i - 1]['id'];
                    $additive['prev_code'] = $additives[$i - 1]['code'];
                    $additive['prev_title'] = $additives[$i - 1]['title'];
                }

                if ($i != count($additives) - 1) {
                    $additive['next_id'] = $additives[$i + 1]['id'];
                    $additive['next_code'] = $additives[$i + 1]['code'];
                    $additive['next_title'] = $additives[$i + 1]['title'];
                }
            }
        }

        foreach ($additive as $key => &$value)
            $value = $this->get_value($key, $value);

        return $additive;
    }

    public function additives($args = array()) {
        $fact = new AdditiveFactory(\core::$db);
        $additives = $fact->all_additives();
        return $additives;
    }

    public function get_value($key, $value) {
        $result = !empty($value) ? $value : '&nbsp;';
        switch ($key) {
            case Additive::IN_EU_KEY:
            case Additive::IN_RU_KEY:
                $result =$this->is_enabled($value);
                break;
            case 'spec':
                $result = nl2br($value);
                break;
        }

        return $result;
    }

    public function is_enabled($value = 0) {
        $result = $value == Additive::ALLOWED ? 'Разрешена.' : 'Запрещена.';
        return $result;
    }
}
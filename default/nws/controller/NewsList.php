<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 05.02.17
 * Time: 11:11
 */
namespace nws\controller;

use nws\model\NewsFactory;

class NewsList extends \Controller {
    public function start() {
        $tpl = &$this->tpl;
        $this->css = array(
          'news_list.css'
        );
        $news = $this->news();

        $this->breadcrumb = true;
        $this->breadcrumbs = $this->breadcrumbs();
        $this->tpl->assign('news_list', $news);
        $this->content = $tpl->fetch('news_list.tpl');
    }

    public function news($args = array()) {
        $fact = new NewsFactory(\core::$db);
        $news = $fact->get_list()->as_array();
        return $news;
    }

    public function breadcrumb_title() {
        return 'Новости проекта';
    }

    public function breadcrumbs($args = array()) {
        $breadcrumbs = array(
//            array('spec' => 'Новости', 'href' => false)
        );
        return $breadcrumbs;
    }
}
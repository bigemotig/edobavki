<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 13.02.17
 * Time: 13:00
 */
namespace nws\model;

class NewsRecord extends \ModelRecord {
    public $id                  = 0;    // int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор новости''
    public $spec                = '';   // varchar(128) NOT NULL COMMENT 'Название новости'и'
    public $announcement        = '';   // text NOT NULL COMMENT 'Анонс новости'
    public $content             = '';   // text DEFAULT NULL COMMENT 'Контент новости'
    public $updater_id          = null; // smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Идентификатор изменившего запись'
    public $update_time         = null; // date DEFAULT NULL COMMENT 'Дата изменения записи'
    public $creator_id          = 0;    // smallint(5) UNSIGNED NOT NULL COMMENT 'Идентификатор создателя записи'
    public $create_time         = '';   // date NOT NULL COMMENT 'Дата создания запи
}

class News extends \Model {
    const LAST_NEWS = 3; //количество последних новостей

    public function __construct($db, $data = null) {
        $this->table = 'nws_news';
        parent::__construct($db, $data);
    }
}

class NewsFactory extends \Factory {
    public function __construct($db) {
        $this->table = 'nws_news';
        parent::__construct($db);
    }

    public function get_list($args = array()) {
        $limit = isset($args['limit']) ? $args['limit'] : 10;
        $sql = new \SqlWriter($this->table);
        $sql
            ->set_field(array('id', 'spec', 'announcement', 'create_time'))
            ->set_limit($limit)
            ->set_orders(array('id' => 'DESC'));

        $args['sql'] = $sql;
        return parent::get_list($args);
    }

    public function last_news($args = array()) {
        $limit = isset($args['limit']) ? $args['limit'] : News::LAST_NEWS;
        $sql = new \SqlWriter($this->table);
        $sql
            ->set_field(array('id', 'spec', 'announcement', 'create_time'))
            ->set_limit($limit)
            ->set_orders(array('id' => 'DESC'));

        $args['sql'] = $sql;
        return parent::get_list($args);
    }
}
<div class="section" id="news_list">
    <div class="col s12 m6" id="news_block">
        <div class="row news-item">
            {foreach from=$news_list item=$news}
                <div class="col s12 offset-m1 m10 offset-l3 l6">
                    <div class="row">
                        <h2 class="left">({$news.create_time}) {$news.spec}</h2>
                    </div>
                    <div class="row">
                        <p class="light">
                            {$news.announcement}
                        </p>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>
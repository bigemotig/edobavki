<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 18.10.15
 * Time: 11:56
 */
define('ROOT', dirname(__FILE__));

if(isset($_SERVER['HTTP_HTTPS']) && $_SERVER['HTTP_HTTPS'] == 'on')
    define('PROTOCOL', 'https');
elseif (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
    define('PROTOCOL', 'https');
else
    define('PROTOCOL', 'http');

define('HOST', $_SERVER['HTTP_HOST']);
define('BASE_URL', PROTOCOL.'://'.HOST); //корень с точки зрения клиента
define('TPATH', BASE_URL . '/default/_common/misc/');

require_once 'config/main.php';
//require_once 'config/config.php';
require_once 'config/server.php';
require_once('libs/smarty/Smarty.class.php');

require_once ('core/init.php');
require_once ('core/Controller.php');
require_once ('core/preloader.php');
require_once ('core/Model.php');
require_once ('core/db.php');
require_once ('core/SqlWriter.php');

$config['mysql']['week_mode'] = 3;
$rm = new RunManager($config);
$rm->init();
$rm->run();